package com.example.spring.todoapp.logic;

import com.example.spring.todoapp.TaskConfigurationProperties;
import com.example.spring.todoapp.model.*;
import com.example.spring.todoapp.model.projection.GroupReadModel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ProjectServiceTest {

    private Project testProject(String projectDescription, Set<Integer> daysToDeadline) {

        Set<ProjectStep> steps = daysToDeadline.stream()
                .map(days -> {
                    var step = mock(ProjectStep.class);
                    when(step.getDescription()).thenReturn("foobar");
                    when(step.getDaysToDeadline()).thenReturn(days);
                    return step;
                }).collect(Collectors.toSet());
        var result = mock(Project.class);
        when(result.getDescription()).thenReturn(projectDescription);
        when(result.getSteps()).thenReturn(steps);
        return result;

    }

    private TaskGroupRepository taskGroupRepositoryReturning(boolean b) {
        var mockTaskGroupRepository = mock(TaskGroupRepository.class);
        when(mockTaskGroupRepository.existsByDoneIsFalseAndProject_Id(anyInt())).thenReturn(b);
        return mockTaskGroupRepository;
    }

    private TaskConfigurationProperties configurationReturning(boolean b) {
        var mockTaskConfigTemplate = mock(TaskConfigurationProperties.Template.class);
        when(mockTaskConfigTemplate.isAllowMultipleTasks()).thenReturn(b);
        var mockTaskConfig = mock(TaskConfigurationProperties.class);
        when(mockTaskConfig.getTemplate()).thenReturn(mockTaskConfigTemplate);
        return mockTaskConfig;
    }

    private InMemoryTaskGroupRepository inMemoryTaskGroupRepository() {
        return new InMemoryTaskGroupRepository();
    }

    private static class InMemoryTaskGroupRepository implements TaskGroupRepository {

        private int index = 0;
        private Map<Integer, TaskGroup> map = new HashMap<>();

        public int count() {
            return map.values().size();
        }

        @Override
        public List<TaskGroup> findAll() {
            return new ArrayList<>(map.values());
        }

        @Override
        public Optional<TaskGroup> findById(Integer id) {
            return Optional.ofNullable(map.get(id));
        }

        @Override
        public TaskGroup save(TaskGroup entity) {
            if (entity.getId() == 0) {
                try {
                    var field = TaskGroup.class.getDeclaredField("id");
                    field.setAccessible(true);
                    field.set(entity, ++index);
                } catch (NoSuchFieldException | IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
            map.put(entity.getId(), entity);
            return entity;
        }

        @Override
        public boolean existsByDoneIsFalseAndProject_Id(Integer projectId) {
            return map.values().stream()
                    .filter(group -> !group.isDone())
                    .anyMatch(group -> group.getProject() != null && group.getProject().getId() == projectId);
        }

    }


    @Test
    @DisplayName("Should throw IllegalStateException when configured to allow 1 group only and the other group undone")
    void createGroupConfigurationOkAndNoProjectsThrowsIllegalStateException() {
        //given
        TaskGroupRepository mockTaskGroupRepository = taskGroupRepositoryReturning(true);
        TaskConfigurationProperties mockTaskConfig = configurationReturning(false);

        ProjectService toTest = new ProjectService(null, mockTaskGroupRepository, null, mockTaskConfig);

        //when
        var exception = catchThrowable(() -> toTest.createGroup(0, LocalDateTime.now()));

        //then
        assertThat(exception).isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("Only one undone group");
    }

    @Test
    @DisplayName("Should throw IllegalArgumentException when configuration ok and no projects with given ID")
    void createGroupConfigurationOkAndNoProjectsThrowsIllegalArgumentException() {

        //given
        var mockProjectRepository = mock(ProjectRepository.class);
        when(mockProjectRepository.findById(anyInt())).thenReturn(Optional.empty());
        TaskConfigurationProperties mockTaskConfig = configurationReturning(true);

        ProjectService toTest = new ProjectService(mockProjectRepository, null, null, mockTaskConfig);

        //when
        var exception = catchThrowable(() -> toTest.createGroup(0, LocalDateTime.now()));

        //then
        assertThat(exception).isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("given id not found");
    }

    @Test
    @DisplayName("Should throw IllegalArgumentException when configured to allow 1 group only and no groups and no projects with given ID")
    void createGroupConfigurationOkAndNoProjectsAndNoGroupsThrowsIllegalArgumentException() {

        //given
        var mockProjectRepository = mock(ProjectRepository.class);
        when(mockProjectRepository.findById(anyInt())).thenReturn(Optional.empty());

        TaskGroupRepository mockTaskGroupRepository = taskGroupRepositoryReturning(false);
        TaskConfigurationProperties taskConfig = configurationReturning(true);

        ProjectService toTest = new ProjectService(mockProjectRepository, mockTaskGroupRepository, null, taskConfig);

        //when
        var exception = catchThrowable(() -> toTest.createGroup(0, LocalDateTime.now()));
        assertThat(exception).isInstanceOf(IllegalArgumentException.class).hasMessageContaining("given id not found");
    }

    @Test
    @DisplayName("Should create new group from project")
    void createGroupConfigurationOkExistingProjectCreateAndSaveNewGroup() {

        //given
        var today = LocalDate.now().atStartOfDay();
        var mockProjectRepository = mock(ProjectRepository.class);
        var project = testProject("foobar", Set.of(-1,-2));
        when(mockProjectRepository.findById(anyInt())).thenReturn(Optional.of(project));
        InMemoryTaskGroupRepository inMemoryTaskGroupRepo = inMemoryTaskGroupRepository();
        var inMemoryTaskGroupService = new TaskGroupService(inMemoryTaskGroupRepo, null);
        int countBeforeCall = inMemoryTaskGroupRepo.count();
        TaskConfigurationProperties taskConfig = configurationReturning(true);
        ProjectService toTest = new ProjectService(mockProjectRepository, inMemoryTaskGroupRepo, inMemoryTaskGroupService, taskConfig);

        //when
        GroupReadModel resultReadModel = toTest.createGroup(1, today);

        //then
        assertThat(resultReadModel.getDescription()).isEqualTo("foobar");
        assertThat(resultReadModel.getDeadline()).isEqualTo(today.minusDays(1));
        assertThat(resultReadModel.getTasks()).allMatch(task -> task.getDescription().equals("foobar"));
        assertThat(countBeforeCall + 1).isEqualTo(inMemoryTaskGroupRepo.count());
    }
}
