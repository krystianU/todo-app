package com.example.spring.todoapp.logic;

import com.example.spring.todoapp.model.TaskGroup;
import com.example.spring.todoapp.model.TaskGroupRepository;
import com.example.spring.todoapp.model.TaskRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TaskGroupServiceTest {

    @Test
    @DisplayName("Group has undone tasks and method throws IllegalStateException")
    void groupHasUndoneTasksAndThrowsIllegalStateException() {

        //given
        var mockTaskRepository = taskRepositoryReturning(true);

        TaskGroupService toTest = new TaskGroupService(null, mockTaskRepository);

        //when
        var exception = catchThrowable(() -> toTest.toggleGroup(0));

        //then
        assertThat(exception).isInstanceOf(IllegalStateException.class).hasMessageContaining("Group has undone tasks");
    }


    @Test
    @DisplayName("Group has all tasks done and method throws Illegal Argument Exception")
    void groupHasCompletedTasksAndThrowsIllegalArgumentException() {
        //given
        var mockTaskRepository = taskRepositoryReturning(false);
        var mockTaskGroupRepository = mock(TaskGroupRepository.class);
        when(mockTaskGroupRepository.findById(anyInt())).thenReturn(Optional.empty());

        TaskGroupService toTest = new TaskGroupService(mockTaskGroupRepository, mockTaskRepository);

        //when
        var exception = catchThrowable(() -> toTest.toggleGroup(0));

        //then
        assertThat(exception).isInstanceOf(IllegalArgumentException.class).hasMessageContaining("given id not found!");

    }

    @Test
    @DisplayName("Group has all tasks done and returned Task Group")
    void groupHasAllCompletedTasksAndShallBeSetAsDone() {

        //when
        var mockTaskRepository = taskRepositoryReturning(false);
        var mockTaskGroupRepository = mock(TaskGroupRepository.class);
        TaskGroup testTaskGroup = new TaskGroup();
        boolean beforeToggle = testTaskGroup.isDone();
        when(mockTaskGroupRepository.findById(anyInt())).thenReturn(Optional.of(testTaskGroup));

        TaskGroupService toTest = new TaskGroupService(mockTaskGroupRepository, mockTaskRepository);
        toTest.toggleGroup(1);

        //then
        assertThat(testTaskGroup.isDone()).isEqualTo(!beforeToggle);

    }

    private TaskRepository taskRepositoryReturning(boolean b) {
        var mockTaskRepository = mock(TaskRepository.class);
        when(mockTaskRepository.existsByDoneIsFalseAndGroup_Id(anyInt())).thenReturn(b);
        return mockTaskRepository;
    }

}