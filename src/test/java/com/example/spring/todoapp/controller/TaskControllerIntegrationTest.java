package com.example.spring.todoapp.controller;

import com.example.spring.todoapp.model.Task;
import com.example.spring.todoapp.model.TaskRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ActiveProfiles("integration")
@SpringBootTest
@AutoConfigureMockMvc
public class TaskControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TaskRepository taskRepository;

    @Test
    void httpGet_returnTaskWithGivenId() throws Exception {
        //given
        int id = taskRepository.save(new Task("foo", LocalDateTime.now())).getId();
        //when + then
        mockMvc.perform(get("/tasks/" + id)).andExpect(status().is2xxSuccessful());
    }

    @Test
    void httpGet_returnAllTasks() throws Exception {
        //given
        int initial = taskRepository.findAll().size();
        taskRepository.save(new Task("foo", LocalDateTime.now()));
        taskRepository.save(new Task("bar", LocalDateTime.now()));

        //when
        MvcResult result = mockMvc.perform(get("/tasks"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(containsString("foo")))
                .andExpect(content().string(containsString("bar")))
                .andReturn();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        List<Task> resultTasks = objectMapper.readValue(result.getResponse().getContentAsString(), new TypeReference<>() {
        });

        //then
        assertThat(resultTasks).hasSize(2 + initial);
    }

    @Test
    void httpPost_saveGivenTask() throws Exception {
        //given
        Task testTask = new Task("foobar", LocalDateTime.now());
        taskRepository.save(testTask);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        //when + then
        mockMvc.perform(post("/tasks")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(testTask)))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("description", is("foobar")));


    }
}
