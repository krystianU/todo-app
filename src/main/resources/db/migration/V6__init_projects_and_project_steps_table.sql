CREATE TABLE projects
(
    id          int primary key auto_increment,
    description varchar(100) not null
);

CREATE TABLE project_steps
(
    id               int primary key auto_increment,
    description      varchar(100),
    days_to_deadline int,
    project_id       int not null,
    foreign key (project_id) references projects (id)
);

ALTER TABLE TASK_GROUPS
    add column project_id int null;
ALTER TABLE TASK_GROUPS
    add foreign key (project_id) references projects (id);

