package com.example.spring.todoapp.model.projection;

import com.example.spring.todoapp.model.Task;
import com.example.spring.todoapp.model.TaskGroup;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

public class GroupTaskWriteModel {

    @NotBlank(message = "Task description too short (must contain at least one character)")
    private String description;
    @DateTimeFormat
    private LocalDateTime deadline;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    Task toTask(final TaskGroup group) {
       return new Task(description, deadline, group);
    }
}

