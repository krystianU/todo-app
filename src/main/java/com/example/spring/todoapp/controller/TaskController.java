package com.example.spring.todoapp.controller;

import com.example.spring.todoapp.model.Task;
import com.example.spring.todoapp.model.TaskRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/tasks")
class TaskController {
    private static final Logger logger = LoggerFactory.getLogger(TaskController.class);
    private final TaskRepository repository;

    public TaskController(final TaskRepository repository) {
        this.repository = repository;
    }

    @GetMapping(params = {"!sort", "!page", "!size"})
    ResponseEntity<List<Task>> readAllTasks() {
        logger.warn("Exposing all tasks!");
        return ResponseEntity.ok(repository.findAll());
    }

    @GetMapping
    ResponseEntity<List<Task>> readAllTasks(Pageable page) {
        logger.info("Custom pageable");
        return ResponseEntity.ok(repository.findAll(page).getContent());
    }

    @GetMapping("/{id}")
    ResponseEntity<Task> getById(@PathVariable("id") int id) {
        logger.info("Getting task by id");
        return repository.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping("/search/done")
    ResponseEntity<List<Task>> findDoneTasks(@RequestParam(defaultValue = "true") boolean state) {
        return ResponseEntity.ok(repository.findByDone(state));
    }

    @GetMapping("/today")
    ResponseEntity<List<Task>> tasksForToday(){
        LocalDate today = LocalDate.now();
        return ResponseEntity.ok(repository.findUndoneTasksDeadlineTodayOrPast(today));
    }

    @PutMapping("/{id}")
    ResponseEntity<?> updateTask(@PathVariable("id") int id, @RequestBody @Valid Task toUpdate) {
        if (!repository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        logger.info("Updating task");
        repository.findById(id).ifPresent(task -> {
            task.updateFrom(toUpdate);
            repository.save(task);
        });

        return ResponseEntity.noContent().build();
    }

    @PostMapping
    ResponseEntity<Task> createTask(@RequestBody @Valid Task toCreate) {
        logger.info("Creating task");
        Task result = repository.save(toCreate);
        return ResponseEntity.created(URI.create("/" + result.getId())).body(result);
    }


    @PatchMapping("/{id}")
    public ResponseEntity<?> toggleTask(@PathVariable("id") int id) {
        if (!repository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        logger.info("Toggling task");
        repository.findById(id).ifPresent(task -> {
            task.setDone(!task.isDone());
            repository.save(task);
        });
        return ResponseEntity.noContent().build();
    }

}
