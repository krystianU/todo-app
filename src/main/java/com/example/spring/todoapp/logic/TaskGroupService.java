package com.example.spring.todoapp.logic;


import com.example.spring.todoapp.model.Project;
import com.example.spring.todoapp.model.TaskGroup;
import com.example.spring.todoapp.model.TaskGroupRepository;
import com.example.spring.todoapp.model.TaskRepository;
import com.example.spring.todoapp.model.projection.GroupReadModel;
import com.example.spring.todoapp.model.projection.GroupWriteModel;

import java.util.List;
import java.util.stream.Collectors;

public class TaskGroupService {

    private TaskGroupRepository groupRepository;
    private TaskRepository taskRepository;

    TaskGroupService(final TaskGroupRepository groupRepository, final TaskRepository taskRepository) {
        this.groupRepository = groupRepository;
        this.taskRepository = taskRepository;
    }

    public GroupReadModel createGroup(final GroupWriteModel source) {
        return createGroup(source, null);
    }

    GroupReadModel createGroup(final GroupWriteModel source, final Project project){
        TaskGroup result = groupRepository.save(source.toGroup(project));
        return new GroupReadModel(result);
    }

    public List<GroupReadModel> readAll() {
        return groupRepository.findAll().stream().map(GroupReadModel::new).collect(Collectors.toList());
    }

    public void toggleGroup(int groupId) {
        if (taskRepository.existsByDoneIsFalseAndGroup_Id(groupId)) {
            throw new IllegalStateException("Group has undone tasks. Complete all tasks first");
        }
        TaskGroup result = groupRepository.findById(groupId)
                .orElseThrow(() -> new IllegalArgumentException("Group with given id not found!"));
        result.setDone(!result.isDone());
        groupRepository.save(result);
    }

}
