package com.example.spring.todoapp.adapter;

import com.example.spring.todoapp.model.Task;
import com.example.spring.todoapp.model.TaskRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
interface SqlTaskRepository extends TaskRepository, JpaRepository<Task, Integer> {

    @Override
    @Query(nativeQuery = true, value = "SELECT COUNT(*) > 0 FROM tasks WHERE id =:id")
    boolean existsById(@Param(value = "id") Integer id);

    @Override
    boolean existsByDoneIsFalseAndGroup_Id(Integer groupId);

    @Override
    List<Task> findAllByGroup_Id(Integer groupId);

    @Override
    @Query(nativeQuery = true, value = "SELECT * FROM TASKS t WHERE DEADLINE <= :date AND done = false")
    List<Task> findUndoneTasksDeadlineTodayOrPast(@Param(value = "date") LocalDate date);
}
